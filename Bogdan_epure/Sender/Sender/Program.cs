﻿using System;
using RabbitMQ.Client;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace rabbit_sender
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "userInsertMsgQ",
                                            durable: false,
                                            exclusive: false,
                                            autoDelete: false,
                                            arguments: null);


                    PatientService _patientService = new PatientService();
                    IList<Patient> objeUserList = _patientService.GetAllPatientACtions();
                    //Patient objeUserList = _patientService.GetAllPatientACtions();

                    //string message = "";


                    //message += JsonConvert.SerializeObject(objeUserList);
                    //var body = Encoding.UTF8.GetBytes(message);

                    for(var i = 0; i < objeUserList.Count; i++)
                    {
                        //var body = Encoding.UTF8.GetBytes(objeUserList[i].StartEndDiff.ToString() + objeUserList[i].Activity);
                        var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(objeUserList[i]));

                        channel.BasicPublish(exchange: "",
                        routingKey: "userInsertMsgQ",
                        basicProperties: null,
                        body: body);
                    }
                    

                }

                Console.WriteLine("press enter to exit");
                Console.ReadLine();
            }
        }
    }
}
