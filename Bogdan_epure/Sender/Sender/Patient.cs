﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rabbit_sender
{
    class Patient
    {
        public string Id { get; set; }
        public string Activity { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }

        public TimeSpan StartEndDiff { get => End - Start; }
    }
}
