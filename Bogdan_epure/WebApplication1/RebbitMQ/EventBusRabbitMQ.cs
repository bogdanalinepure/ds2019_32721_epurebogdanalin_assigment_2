﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using Microsoft.AspNetCore.Http;
using NetCoreAngular.Controllers;

namespace RabbitMQReiverCoreAPI.RebbitMQ
{
    public class EventBusRabbitMQ : Controller, IDisposable
    {
        private readonly IRabbitMQPersistentConnection _persistentConnection;
        private IModel _consumerChannel;
        private string _queueName;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public EventBusRabbitMQ(IRabbitMQPersistentConnection persistentConnection,  IHttpContextAccessor httpContextAccessor, string queueName = null)
        {
            _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
            _queueName = queueName;
            _httpContextAccessor = httpContextAccessor;
        }

        [Route("userInsertMsgQ")]
        public IModel CreateConsumerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }
            //var ws = HttpContext.WebSockets.AcceptWebSocketAsync();

            var channel = _persistentConnection.CreateModel();
            channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            //Create event when something receive
            consumer.Received += ReceivedEvent;

            channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };
            return channel;
        }

        private async void ReceivedEvent(object sender, BasicDeliverEventArgs e)
        {
            if (e.RoutingKey == "userInsertMsgQ")
            {
                var message = Encoding.UTF8.GetString(e.Body);
                //var deserialized = JsonConvert.DeserializeObject(message);
                //var createModel = new MessageModel();
                //createModel.Message.Add(deserialized.ToString());
                ArraySegment<byte> bytesMessage = Encoding.ASCII.GetBytes(message);

                //var ws = await HttpContext.WebSockets.AcceptWebSocketAsync();
                //var ws = _httpContextAccessor.HttpContext.WebSockets.AcceptWebSocketAsync();
                
                //await ws. .SendAsync(bytesMessage, WebSocketMessageType.Binary, true, CancellationToken.None);



                var context = ControllerContext.HttpContext;
                //WebSocketAcceptContext webSocket = new WebSocketAcceptContext();
                //WebSocketContext context = new WebSocketContext();
                //if (context.WebSockets.IsWebSocketRequest)
                //{
                //    WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                //    await webSocket.SendAsync(bytesMessage, WebSocketMessageType.Binary, true, CancellationToken.None);
                //}
                //List<Patient> userList = JsonConvert.DeserializeObject<List<Patient>>(message);
            }
        }


        public new void Dispose()
        {
            if (_consumerChannel != null)
            {
                _consumerChannel.Dispose();
            }
        }
    }
}


