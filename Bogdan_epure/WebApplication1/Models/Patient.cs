﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreAngular.Models
{
    class Patient
    {
        public string Id { get; set; }
        public string Activity { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
    }
}
