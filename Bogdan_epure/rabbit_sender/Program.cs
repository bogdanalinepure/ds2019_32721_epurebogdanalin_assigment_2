﻿//using System;
//using RabbitMQ.Client;
//using System.Text;

//namespace rabbit_sender
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            var factory = new ConnectionFactory() { HostName = "localhost" };
//            using (var connection = factory.CreateConnection())
//            {
//                using (var channel = connection.CreateModel())
//                {
//                    channel.QueueDeclare(queue: "hello",
//                                            durable: false,
//                                            exclusive: false,
//                                            autoDelete: false,
//                                            arguments: null);

//                    string message = "This is the data that i want to send";
//                    var body = Encoding.UTF8.GetBytes(message);


//                    channel.BasicPublish(exchange: "",
//                        routingKey: "hello",
//                        basicProperties: null,
//                        body: body);

//                    Console.WriteLine("[x] SenT  {0}", message);
//                }

//                Console.WriteLine("press enter to exit");
//                Console.ReadLine();
//            }
//        }
//    }
//}

using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace rabbit_sender
{
    class Program
    {

        //packege manager console > Install-Package RabbitMQ.Client
        //packege manager console > Install-Package Newtonsoft.json

        static void Main(string[] args)
        {
            Console.WriteLine("Hello this is the sender application!");
            string senderUniqueId = "userInsertMsgQ";

            var factory = new ConnectionFactory() { HostName = "localhost", UserName = "guest", Password = "guest" };
            var connection = factory.CreateConnection();

            //-------------------------  Sending Data --------------------------------------------------------------------------------------
            #region Sending Data
            using (var channel = connection.CreateModel())
            {

                channel.QueueDeclare(queue: "userInsertMsgQ", durable: false, exclusive: false, autoDelete: false, arguments: null);

                Console.WriteLine("Type something and press 'Enter Button' to send user list");
                Console.ReadLine();

                // create serialize object to send
                PatientService _patientService = new PatientService();
                List<Patient> objeUserList = _patientService.GetAllPatientACtions();
                string message = JsonConvert.SerializeObject(objeUserList);

                var body = Encoding.UTF8.GetBytes(message);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                properties.DeliveryMode = 2;
                properties.Headers = new Dictionary<string, object>();
                properties.Headers.Add("senderUniqueId", senderUniqueId);//optional unique sender details in receiver side              
                // properties.Expiration = "36000000";
                //properties.ContentType = "text/plain";

                channel.ConfirmSelect();

                channel.BasicPublish(exchange: "",
                                     routingKey: "userInsertMsgQ",
                                     basicProperties: properties,
                                     body: body);

                channel.WaitForConfirmsOrDie();

                channel.BasicAcks += (sender, eventArgs) =>
                {
                    Console.WriteLine("Sent RabbitMQ");
                    //implement ack handle
                };
                channel.ConfirmSelect();

            }
            #endregion
            
        }
    }
}
