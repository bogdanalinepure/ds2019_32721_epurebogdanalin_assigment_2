﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace rabbit_sender
{
    internal class PatientService
    {
        public PatientService()
        {
        }

        enum ActivitiesToWatch
        {
            Sleeping = 0,
            Leaving = 1,
            Toileting = 2
        }

        public IList<Patient> GetAllPatientACtions()
        {
            IList<Patient> patientList = new List<Patient>();
            IList<Patient> result = new List<Patient>();

            patientList = ReadFile();

            foreach(var line in patientList)
            {
                if (line.Activity == ActivitiesToWatch.Sleeping.ToString() && line.StartEndDiff > new TimeSpan(12))
                {
                    result.Add(line);
                }
            }
            return result;
        }


        internal IList<Patient> ReadFile()
        {

            var lines = File.ReadLines(@"C:\activity.txt");
            List<Patient> buffer = new List<Patient>();
            
            foreach (var line in lines)
            {
                string replacement = Regex.Replace(line, @"\t|\n|\r", " ");
                var temp = new Patient{
                    Start = TimeSpan.Parse(replacement.Split(" ")[1]),
                    End = TimeSpan.Parse(replacement.Split(" ")[4]),
                    Activity = replacement.Split(" ")[6],
                    Id = Guid.NewGuid().ToString(),
                };
                buffer.Add(temp);
            }
            return buffer;
        }
    }
}
