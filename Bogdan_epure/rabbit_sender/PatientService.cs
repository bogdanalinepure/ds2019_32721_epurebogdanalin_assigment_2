﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rabbit_sender
{
    internal class PatientService
    {
        public PatientService()
        {
        }

        internal List<Patient> GetAllPatientACtions()
        {
            List<Patient> patientList = new List<Patient>;
            patientList.Add(new Patient { Id = "1", ACtivity = "sleeping", End = "12312312312", Start = "22131233123" });
            patientList.Add(new Patient { Id = "2", ACtivity = "TV", End = "12312312312", Start = "22131233123" });
            patientList.Add(new Patient { Id = "3", ACtivity = "WAlking", End = "12312312312", Start = "22131233123" });
            patientList.Add(new Patient { Id = "4", ACtivity = "Grooming", End = "12312312312", Start = "22131233123" });

            return patientList;
        }
    }
}
