﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NetCoreAngular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [Route("userInsertMsgQ")]
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public void Connector(ArraySegment<byte> variable)
        {

        }

        //[HttpGet]
        //public async Task ReceiveWSMessage(new ArraySegment<byte>(buffer))
        //{
        //    WebSocket webSocket = await ControllerContext.HttpContext.WebSockets.AcceptWebSocketAsync();

        //    var bytesMessage = new byte[1024];

        //    //ArraySegment<byte> bytesMessage = Encoding.ASCII.GetBytes(message);
        //    //bytesMessage = Encoding.ASCII.GetBytes(new String(buffer, "UTF-8").ToCharArray());

        //    //await webSocket.ReceiveAsync(new ArraySegment<byte>(bytesMessage), CancellationToken.None);
        //    await webSocket.SendAsync(new ArraySegment<byte>(buffer.ToArray<byte>()),null, cancellationToken: CancellationToken.None);
        //}
    }
}