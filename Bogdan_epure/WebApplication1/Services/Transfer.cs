﻿using Microsoft.AspNetCore.Http;
using NetCoreAngular.IServices;
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace NetCoreAngular.Services
{
    public class Transfer: ITransfer
    {
        private readonly HttpContext _context;

        public Transfer(HttpContext context)
        {
            _context = context;
        }

        public async Task Get()
        {
            var buffer = new byte[1024 * 4];
            var ws = await _context.WebSockets.AcceptWebSocketAsync();
            await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            //return items;
        }

        public async Task Send(ArraySegment<byte> buffer)
        {
            var ws = await _context.WebSockets.AcceptWebSocketAsync();
            await ws.SendAsync(buffer, WebSocketMessageType.Binary, true, CancellationToken.None);
            //return res;
        }
    }
}
